Auditd

Auditd is the auditing component of Linux. It is a very powerful and flexible tool, which can be configured to detect many kinds of suspicious or abnormal activity on a system.

## Installation

To install Auditd:

Ensure the system is up to date:
`sudo apt update && sudo apt upgrade -y`
Next, install Auditd:

`sudo apt install auditd audispd-plugins`

## Configuration

The auditd configuration file is located at `/etc/audit/auditd.conf`.

The configuration used is a modified version of the following configuration:

https://github.com/Neo23x0/auditd

Ippsec has a detailed video going over each line in the configuration:

https://www.youtube.com/watch\?v\=lc1i9h1GyMA

The following are some options that can be of interest:

`log_file`: This is the location where audit logs will be stored. By default, it is set to /var/log/audit/audit.log.

`log_format`: Specifies the log format, choose either RAW or NOLOG. The default is RAW.

`log_group`: The group that owns the log files. By default, it is set to root.

`priority_boost`: Set the priority boost to increase the priority of the audit daemon. The default is 4.

`flush`: Determines how auditd writes data to disk. Options are INCREMENTAL, INCREMENTAL_ASYNC, DATA, SYNC, or NONE. The default is INCREMENTAL.

`freq`: Specifies the number of records to write before invoking a flush operation. The default is 20.

`num_logs`: Specifies the number of log files to retain. The default is 5.

`disp_qos`: Determines the quality of service for event delivery between the kernel and audit daemon. Options are lossy or lossless. The default is lossy.

`dispatcher`: Specifies the path to the dispatcher program. By default, it is set to /sbin/audispd.

`name_format`: Determines how the hostname is stored in the audit logs. Options are NONE, FQD, NUMERIC, or USER. The default is NONE.

## Audit Rules

Auditd is configured by adding rules to `/etc/audit/rules.d/`. https://github.com/Neo23x0/auditd can be used as a very good reference when making these rules.

The syntax is as follows:

For monitoring files or directories:

`-w /path/to/file -p [permissions] -k [key-name]`

For tracking specific system calls:

`-a [action], [filter] -S [syscall] -k [key-name]`

Here are two example rules to show the syntax of creating rules:
````
  # Monitor file access to a specific file
 -w /path/to/your/file -p wa -k file_access
  # Monitor changes to the /etc/passwd file
 -w /etc/passwd -p wa -k passwd_changes
````

## Audit Plugins

Auditd supports plugins through the audisp-plugins feature. One example of a plugin is incoorperating rsyslog with auditd, through the audisp-syslog plugin.

## Using Auditd

### Ausearch

ausearch is used to search and filter the audit logfile for events. Some options include:

`-k [key-name]`: Search for events with the specified key.
`-ts [timestamp]`: Search for events after the specified timestamp.
`-te [timestamp]`: Search for events before the specified timestamp.
`-x [executable]`: Search for events related to the specified executable.

To check for i.e reconnaissance on a system:

`sudo ausearch -k recon -i`

- `-i` - Interprets numeric entries into text (human-readable output).

Example output:

````
----
type=PROCTITLE msg=audit(04/24/2023 12:32:19.214:255601) : proctitle=whoami 
type=PATH msg=audit(04/24/2023 12:32:19.214:255601) : item=1 name=/lib64/ld-linux-x86-64.so.2 inode=4991626 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0 
type=PATH msg=audit(04/24/2023 12:32:19.214:255601) : item=0 name=/usr/bin/whoami inode=4981960 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0 
type=EXECVE msg=audit(04/24/2023 12:32:19.214:255601) : argc=1 a0=whoami 
type=SYSCALL msg=audit(04/24/2023 12:32:19.214:255601) : arch=x86_64 syscall=execve success=yes exit=0 a0=0x5586d0e6b640 a1=0x5586d0e6f7c0 a2=0x5586d0df7860 a3=0x8 items=2 ppid=204522 pid=209758 auid=sshuser uid=mayaad0 gid=mayaad0 euid=mayaad0 suid=mayaad0 fsuid=mayaad0 egid=mayaad0 sgid=mayaad0 fsgid=mayaad0 tty=pts0 ses=22 comm=whoami exe=/usr/bin/whoami subj=unconfined key=recon 
----
type=PROCTITLE msg=audit(04/24/2023 12:32:21.270:255602) : proctitle=uname -a 
type=PATH msg=audit(04/24/2023 12:32:21.270:255602) : item=1 name=/lib64/ld-linux-x86-64.so.2 inode=4991626 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0 
type=PATH msg=audit(04/24/2023 12:32:21.270:255602) : item=0 name=/usr/bin/uname inode=4981908 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0 
type=EXECVE msg=audit(04/24/2023 12:32:21.270:255602) : argc=2 a0=uname a1=-a 
type=SYSCALL msg=audit(04/24/2023 12:32:21.270:255602) : arch=x86_64 syscall=execve success=yes exit=0 a0=0x5586d0eb16f0 a1=0x5586d0f29330 a2=0x5586d0df7860 a3=0x8 items=2 ppid=204522 pid=209759 auid=sshuser uid=mayaad0 gid=mayaad0 euid=mayaad0 suid=mayaad0 fsuid=mayaad0 egid=mayaad0 sgid=mayaad0 fsgid=mayaad0 tty=pts0 ses=22 comm=uname exe=/usr/bin/uname subj=unconfined key=recon 
````

Explaination of the output:

`type=PROCTITLE msg=audit(04/24/2023 12:32:19.214:255601) : proctitle=whoami`: This line shows the process title of the executed command, which is whoami.

`type=PATH msg=audit(04/24/2023 12:32:19.214:255601) : item=1 name=/lib64/ld-linux-x86-64.so.2 inode=4991626 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0`: This line shows the path of the dynamic linker used by the executed command.

`type=PATH msg=audit(04/24/2023 12:32:19.214:255601) : item=0 name=/usr/bin/whoami inode=4981960 dev=fd:01 mode=file,755 ouid=root ogid=root rdev=00:00 nametype=NORMAL cap_fp=none cap_fi=none cap_fe=0 cap_fver=0 cap_frootid=0`: This line shows the path of the executed command, which is /usr/bin/whoami.

`type=EXECVE msg=audit(04/24/2023 12:32:19.214:255601) : argc=1 a0=whoami`: This line shows the arguments of the executed command, which is whoami.

`type=SYSCALL msg=audit(04/24/2023 12:32:19.214:255601) : arch=x86_64 syscall=execve success=yes exit=0 a0=0x5586d0e6b640 a1=0x5586d0e6f7c0 a2=0x5586d0df7860 a3=0x8 items=2 ppid=204522 pid=209758 auid=sshuser uid=mayaad0 gid=mayaad0 euid=mayaad0 suid=mayaad0 fsuid=mayaad0 egid=mayaad0 sgid=mayaad0 fsgid=mayaad0 tty=pts0 ses=22 comm=whoami exe=/usr/bin/whoami subj=unconfined key=recon`: This line shows the system call made to execute the command, including its arguments and the process information, such as the user ID of the executing user, the parent process ID, and the session ID. The success field indicates that the system call was successful, and the exit field indicates the exit code of the command.

### Aureport

aureport can be used to create a short summarization of the audit report:

````
Summary Report
======================
Range of time in logs: 04/24/2023 12:18:14.723 - 04/24/2023 12:38:25.009
Selected time for report: 04/24/2023 12:18:14 - 04/24/2023 12:38:25.009
Number of changes in configuration: 0
Number of changes to accounts, groups, or roles: 6
Number of logins: 0
Number of failed logins: 0
Number of authentications: 1
Number of failed authentications: 0
Number of users: 2
Number of terminals: 10
Number of host names: 11
Number of executables: 58
Number of commands: 136
Number of files: 18798
Number of AVC's: 0
Number of MAC events: 0
Number of failed syscalls: 15199
Number of anomaly events: 0
Number of responses to anomaly events: 0
Number of crypto events: 0
Number of integrity events: 0
Number of virt events: 0
Number of keys: 29
Number of process IDs: 883
Number of events: 34210
````
